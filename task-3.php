<?php
require_once __DIR__ . '/db/DbDecorator.class.php';

$companyId = 1;
$balance = new Campaign($companyId);
$messageCount = 20;
$balance->checkBalance($messageCount);


class Campaign
{
    private $companyId;

    /** @var DbDecorator */
    private $db;

    public function __construct($companyId)
    {
        $this->companyId = $companyId;
        $this->db = DbDecorator::getInstance();
    }

    public function checkBalance($messageCount)
    {

        $smsLocationId = $this->db->fetchOne("SELECT sms_location_id FROM company WHERE id = {$this->companyId}");

        $balance = $this->db->fetchOne("SELECT balance FROM company WHERE id={$this->companyId}");

        $price = $this->db->fetchOne("SELECT price FROM sms_location WHERE id={$smsLocationId}");


        // Забрали с него нужное кол-во денег (цена смс-ки * кол-во)
        $balance = $balance - $messageCount * $price;

        // Вернули то, что вышло
        return $balance;
    }
}

