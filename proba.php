<?php

class MyClass
{
    public function kvadrat($x)
    {
        return $x * $x;
    }

    public function echoKvadrat()
    {
        echo $this->kvadrat(3) . "\n";
    }
}

