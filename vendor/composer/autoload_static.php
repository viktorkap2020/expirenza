<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5eeabb1a9271ead916440cf0edd9f49f
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PhpQuery\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PhpQuery\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpquery/phpquery/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5eeabb1a9271ead916440cf0edd9f49f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5eeabb1a9271ead916440cf0edd9f49f::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
