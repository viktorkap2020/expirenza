<?php

$parser = new Parser();
$parser->process();


class Parser
{
    const LAST_DATE_FILEPATH = __DIR__ . '/last-date.txt';

    public function process()
    {
        $allReviews = [];

        $allReviews = array_merge($allReviews, $this->getDataFromOnePage(1));

        $lastDateFromSite = $allReviews[0]['date'];
        $lastDateFromFile = $this->getLastDateFromFile();
        $this->writeLastDateToFile($lastDateFromSite);

        if ($lastDateFromFile == $lastDateFromSite) {
            echo "Ничего не поменялось, заканчиваем работу\n";
            return;
        }

        $currentPageReviews = $allReviews; // сейчас так можно, потому что в $allReviews данные из одной страницы

        $pageIndex = 2;
        $nextPageReviews = $this->getDataFromOnePage($pageIndex);

        while ($currentPageReviews != $nextPageReviews) {
            echo "Выкачали страницу {$pageIndex}\n";

            $allReviews = array_merge($allReviews, $nextPageReviews);
            $currentPageReviews = $nextPageReviews;
            $pageIndex++;
            $nextPageReviews = $this->getDataFromOnePage($pageIndex);
        }

        $this->sendToMail($allReviews);
    }

    /**
     * @param $pageIndex int
     * @return array - массив отзывов
     */
    function getDataFromOnePage($pageIndex)
    {
        $contents = file_get_contents("https://www.fps.com.ua/vidguky/?thread_id=26&selected_section=discussion&page={$pageIndex}");

        $dom = new DOMDocument();

        // "Собака" в РНР означает подавление сообщений об ошибках
        @$dom->loadHTML($contents);

        $reviews = [];

        foreach (['message', 'author', 'date'] as $param) {
            foreach ($this->getNodesByClass('ty-discussion-post__' . $param, $dom) as $key => $domElement) {
                $reviews[$key][$param] = $domElement->textContent;
            }
        }

        return $reviews;
    }

    /**
     * @param $allReviews - массив
     */
    public function sendToMail($allReviews)
    {
        $jsonEncodedReviews = json_encode($allReviews, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        mail('notify.bot.zabbix.2@gmail.com', '', $jsonEncodedReviews);
    }

    public function getLastDateFromFile()
    {
        return file_exists(self::LAST_DATE_FILEPATH)
            ? file_get_contents(self::LAST_DATE_FILEPATH)
            : '';
    }

    /** $lastDateFromSite - строка, например "2020-01-30" */
    public function writeLastDateToFile($lastDateFromSite)
    {
        file_put_contents(self::LAST_DATE_FILEPATH, $lastDateFromSite);
    }

    /**
     * @param string $className
     * @param DOMDocument $dom
     * @return DOMNodeList|false
     */
    public function getNodesByClass($className, $dom)
    {
        $finder = new DomXPath($dom);
        $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' {$className} ')]");
        return $nodes;
    }
}