
/* таблица company */
CREATE TABLE company(
    id INT NOT NULL AUTO_INCREMENT,
    sms_location_id INT,
    title VARCHAR(64),
    PRIMARY KEY (id)
);

INSERT INTO company(sms_location_id, title) VALUES (2, 'ООО Рога и Копыта'),
                                                    (7, '«Сделай сам»'),
                                                    (3, 'Супермаркет семейный'),
                                                    (4, 'АТБ'),
                                                    (4, 'Сильпо'),
                                                    (6, 'Клиника «Ваш доктор»');


ALTER TABLE company ADD balance INT;

UPDATE company SET `balance` = 100 WHERE id = 1;
UPDATE company SET `balance` = 200 WHERE id = 2;
UPDATE company SET `balance` = 150 WHERE id = 3;
UPDATE company SET `balance` = 200 WHERE id = 4;
UPDATE company SET `balance` = 150 WHERE id = 5;
UPDATE company SET `balance` = 200 WHERE id = 6;


/* таблица sms_location */

CREATE TABLE sms_location(
    id INT,
    parent_id INT,
    title VARCHAR(64),
    PRIMARY KEY (id)
);

INSERT INTO sms_location VALUES (1, null, 'Globe'),
                                (2, 1, 'Ukraine'),
                                (3, 1, 'USA'),
                                (4, 2, 'Kyiv'),
                                (5, 3, 'New York'),
                                (6, 5, 'Brighton Beach'),
                                (7, 3, 'Dnipro');

ALTER TABLE sms_location ADD price DECIMAL(4, 2);

UPDATE sms_location SET `price` = 10 WHERE id = 1;
UPDATE sms_location SET `price` = 0.35 WHERE id = 2;
UPDATE sms_location SET `price` = 0.05 WHERE id = 3;